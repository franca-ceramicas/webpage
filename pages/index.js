import React from 'react'
import Head from 'next/head'
import NextImage from 'next/image'
import classNames from 'classnames'
import useEmblaCarousel from 'embla-carousel-react'
import { useTranslation } from 'next-i18next'

import ContactUs from '../components/ContactForm'
import ArrowDown from '../assets/arrow-down.svg'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';


export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'contact-form'])),
    },
  }
}

const Block = props => {
  return (
    <div
      id={props?.id}
      className={classNames(
        'py-32 px-10 sm:p-10 lg:p-20',
        'flex flex-col',
        'lg:min-h-screen',
        { 'items-center justify-center': props.center },
        props.className,
      )}
    >
      {props.children}
    </div>
  )
}


export const Button = props => {
  const Component = props.as || 'a'
  return (
    <Component
      href={props?.href || '/'}
      onClick={e => {
        // e.preventDefault()
        props.onClick && props.onClick(e)
      }}
      className={classNames(
        'font-semibold',
        'py-3 px-5 rounded-full transition-all transform hover:scale-105',
        'shadow-md hover:shadow-lg',
        props.className,
      )}
      {...props.buttonProps}
    >
      {props.children}
    </Component>
  )
}


export const ButtonBlue = props => {
  return (
    <Button
      {...props}
      className={classNames(
        props.className,
        'bg-blue-light',
        'hover:bg-blue hover:text-white',
      )}
    />
  )
}


export const ButtonPink = props => {
  return (
    <Button
      {...props}
      className={classNames(
        props.className,
        'bg-pink-light',
        'hover:bg-blue hover:text-white',
      )}
    />
  )
}


const Title = props => {
  const Component = props.as || 'h2'
  return (
    <Component
      className={classNames(
        'font-display',
        'text-5xl lg:text-8xl',
        'text-center font-semibold',
        props.className,
      )}
    >
      {props.children}
    </Component>
  )
}


const Content = props => {
  const Component = props.as || 'div'
  return (
    <Component className={classNames(
      'p-10',
      props.className,
    )}>
      {props.children}
    </Component>
  )
}


const Image = props => {
  const { as, style, asProps = {}, className, imageProps } = props
  const Component = as || 'picture'

  return (
    <Component
      {...asProps}
      style={style}
      className={classNames(
        className,
        'relative group transform transition-all',
      )}
    >
      <NextImage
        layout='fill'
        {...imageProps}
        className={classNames(imageProps?.className, 'h-full relative')}
        alt={imageProps?.alt}
      />
    </Component>
  )
}


const FullHeight = props => {
  return (
    <div className={classNames(
      'lg:min-h-screen',
      props.className,
    )}>
      {props.children}
    </div>
  )
}

const FullHeight2Cols = props => {
  return (
    <FullHeight className={classNames(
      'flex flex-col lg:grid lg:grid-rows-1 lg:grid-cols-2',
      props.className,
    )}>
      {props.children}
    </FullHeight>
  )
}


export const PrevButton = ({ onClick }) => (
  <button
    className='absolute top-1/2 left-0 transform -translate-y-1/2 embla__button embla__button--prev'
    onClick={onClick}
    style={{
      filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, .2))'
    }}
  >
    <ArrowDown className='rotate-90 text-white fill-current transform' />
  </button>
)

export const NextButton = ({ onClick }) => (
  <button
    className='absolute top-1/2 right-0 transform -translate-y-1/2 embla__button embla__button--next'
    onClick={onClick}
    style={{
      filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, .2))'
    }}
  >
    <ArrowDown className='-rotate-90 text-white fill-current transform' />
  </button>
)


const Gallery = props => {
  const { className, items } = props
  const [emblaRef, embla] = useEmblaCarousel({ loop: true })

  const scrollPrev = React.useCallback(() => {
    embla && embla.scrollPrev()
  }, [embla]) // eslint-disable-line
  const scrollNext = React.useCallback(() => {
    embla && embla.scrollNext()
  }, [embla]) // eslint-disable-line

  return (
    <div
      ref={emblaRef}
      className={classNames(
        'embla relative overflow-hidden min-h-screen w-full h-full max-h-screen m-auto bg-white',
        className,
      )}
    >
      <div className='embla__container h-full min-h-screen relative whitespace-nowrap'>
        {
          items.map((item, index) => (
            <span
              className='embla__slide w-10/12 h-full min-h-screen relative inline-block'
              key={`gallery-item-${index}`}
            >
              <NextImage
                layout='fill'
                className='object-cover'
                src={item.src}
                alt={`gallery item ${index}`}
              />
            </span>
          ))
        }
      </div>
      <PrevButton onClick={scrollPrev} />
      <NextButton onClick={scrollNext} />
    </div>
  )
}


function App() {
  const { t, i18n } = useTranslation()

  return (
    <>
      <Head>
        <title>Franca Cerámicas</title>
        <meta name='description' content='Piezas de cerámica de autor.' />
        <link rel='icon' href='/favicon.ico' />
        <link rel='preconnect' href='https://fonts.googleapis.com' />
        <link rel='preconnect' href='https://fonts.gstatic.com' crossOrigin />
        <link href='https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&family=Open+Sans:wght@300;400;600&display=swap' rel='stylesheet' />
      </Head>
      <div
        style={{
          filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, .2))'
        }}
        className='flex fixed bottom-5 right-5 gap-3 z-50'
      >
        <a
          href={i18n.language === 'en' ? 'es' : 'en'}
          rel='noreferrer'
          title='English'
          className='
            flex-grow
            rounded-lg bg-pink
            font-bold
            min-w-[30px] h-[39px] flex items-center justify-center
            px-[10px]
            transition-all transform hover:scale-110
            group
          '
        >
          {
            i18n.language === 'en' ?
              <>ES<span className='hidden group-hover:block'>PAÑOL</span></> :
              <>EN<span className='hidden group-hover:block'>GLISH</span></>
          }
        </a>
        <a
          href='https://www.instagram.com/franca.ceramicas'
          target='_blank'
          rel='noreferrer'
          className='flex-grown z-50 transition-all transform hover:scale-110'
        >
          <Image
            imageProps={{
              layout: '',
              src: '/assets/imgs/icon-instagram.svg',
              width: '40px',
              height: '40px',
              alt: 'Franca Cerámicas - Instagram',
            }}
          />
        </a>
        <a
          href='https://api.whatsapp.com/send?phone=5492616861640&text=Hola!%20Te%20escribo%20desde%20el%20sitio%20web!'
          target='_blank'
          rel='noreferrer'
          className='transition-all transform hover:scale-110'
        >
          <Image
            imageProps={{
              layout: '',
              src: '/assets/imgs/wapp.svg',
              width: '40px',
              height: '40px',
              alt: 'Franca Cerámicas',
            }}
          />
        </a>
      </div>

      <div
        className='fixed bottom-0 right-1/2 z-30 transform translate-x-1/2'
        onClick={() => {
          document.querySelector('body').scrollTop()
        }}
      >
        <ArrowDown
          style={{
            filter: 'drop-shadow(0 1px 2px rgba(0, 0, 0, .2))'
          }}
          className='text-white fill-current animate-bounce'
        />
      </div>
      <FullHeight2Cols className='
        from-blue-light
        to-pink-light
        bg-gradient-to-tr
      '>
        <Block center className='
          flex-grow
          flex-1 min-w-max p-10 text-blue
          h-screen min-h-screen sm:h-auto sm:min-h-0
        '>
          <NextImage
            src='/assets/imgs/logo.svg'
            width='200px'
            height='200px'
            layout=''
          />
          <p className='text-4xl mt-10 font-display'>
            {t('franca-subtitle')}
          </p>
        </Block>
        <Image
          className='h-96 sm:h-screen sm:max-h-screen'
          imageProps={{
            className: 'object-contain object-bottom min-w-0',
            src: '/assets/imgs/hand-plates.png',
            alt: 'Hands and plates',
          }}
        />
      </FullHeight2Cols>
      <FullHeight2Cols className='flex-col-reverse'>
        <Image
          className='h-96 sm:h-screen sm:max-h-screen row-start-2 lg:row-start-1'
          imageProps={{
            className: 'h-full sm:object-top w-full object-cover',
            src: '/assets/imgs/gabrielasimon.jpeg',
            alt: 'Glaces',
          }}
        />
        <Block id='welcome' center className='
          from-blue-light to-green-lightest bg-gradient-to-tr text-black
        '>
          <Title className='text-blue'>
            {t('welcome')}
          </Title>
          <Content className='text-xl text-center'>
            <div dangerouslySetInnerHTML={{ __html: t('welcome-description') }} />
          </Content>
          <ButtonPink href='#about-me'>
            {t('welcome-button')}
          </ButtonPink>
        </Block>
      </FullHeight2Cols>
      <FullHeight2Cols>
        <Block id='about-me' center className='to-pink from-blue-light bg-gradient-to-tl text-black'>
          <Title className='text-blue'>
            {t('about-me-title')}
          </Title>
          <Content className='text-center text-xl'>
            <div dangerouslySetInnerHTML={{ __html: t('about-me-description') }} />
          </Content>
          <ButtonBlue href='#some-pieces'>{t('about-me-button')}</ButtonBlue>
        </Block>
        <Image
          className='h-96 sm:h-screen sm:max-h-screen'
          imageProps={{
            className: 'w-full h-full object-cover',
            src: '/assets/imgs/vases-teapot.jpeg',
            alt: 'Vases & Teapot',
          }}
        />
      </FullHeight2Cols>
      <FullHeight2Cols className='
        flex-col-reverse
        to-white from-gray bg-gradient-to-tl
      '>
        <Image
          className='h-96 sm:h-screen sm:max-h-screen row-start-2 lg:row-start-1 min-h-full'
          imageProps={{
            className: 'object-contain object-bottom min-w-0 w-full max-h-screen',
            src: '/assets/imgs/hand-plate.png',
            alt: 'Hands and plates',
          }}
        />
        <Block center id='contact'>
          <Title className='text-blue'>
            {t('contact-title')}
          </Title>
          <Content className='text-center text-xl'>
            <div dangerouslySetInnerHTML={{ __html: t('contact-description') }} />
          </Content>
          <ContactUs />
        </Block>
      </FullHeight2Cols>

      {/* Ǵallery */}
      <FullHeight2Cols className='
        z-40 relative
        bg-gradient-to-tr
        from-blue-light to-white
      '>
        <Block id='some-pieces' center>
          <Title className='text-blue'>{t('products-title')}</Title>
        </Block>
        <Gallery
          className='shadow-lg'
          items={[...Array(19).keys()].map(
            item => {
              const padNumber = ('000' + (item + 1)).slice(-3)
              return {
                src: `/assets/imgs/products/pic-${padNumber}.jpeg`,
              }
            }
          )}
        />
      </FullHeight2Cols>
    </>
  )
}

export default App
