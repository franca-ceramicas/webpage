import React from 'react'
import { useTranslation } from 'next-i18next';

import { ButtonPink } from '../../pages'


export default function ContactUs() {
  const { t } = useTranslation('contact-form')
  // States for contact form fields
  const [fullname, setFullname] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [subject, setSubject] = React.useState('')
  const [message, setMessage] = React.useState('')

  //   Form validation state
  const [errors, setErrors] = React.useState({})

  // Setting success or failure messages states
  const [showSuccessMessage, setShowSuccessMessage] = React.useState(false)
  const [showFailureMessage, setShowFailureMessage] = React.useState(false)

  // Validation check method
  const handleValidation = () => {
    let tempErrors = {}
    let isValid = true

    if (fullname.length <= 0) {
      tempErrors['fullname'] = true
      isValid = false
    }
    if (email.length <= 0) {
      tempErrors['email'] = true
      isValid = false
    }
    if (subject.length <= 0) {
      tempErrors['subject'] = true
      isValid = false
    }
    if (message.length <= 0) {
      tempErrors['message'] = true
      isValid = false
    }

    setErrors({ ...tempErrors })
    console.log('errors', errors)
    return isValid
  }

  //   Handling form submit

  const handleSubmit = async (e) => {
    e.preventDefault()

    let isValidForm = handleValidation()

    if (isValidForm) {
      const res = await fetch('/api/sendgrid', {
        body: JSON.stringify({
          email: email,
          fullname: fullname,
          subject: subject,
          message: message,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
      })

      const { error } = await res.json()
      if (error) {
        console.log(error)
        setShowSuccessMessage(false)
        setShowFailureMessage(true)
        return
      }
      setShowSuccessMessage(true)
      setShowFailureMessage(false)
    }
    console.log(fullname, email, subject, message)
  }

  return (
    <form
      onSubmit={handleSubmit}
      className='flex flex-col'
    >
      <h1 className='text-2xl font-bold'>
        {t('contact-form-title')}
      </h1>

      <label
        className='mt-8'
      >
        {t('contact-form-name')} <span className='text-red'>*</span>
      </label>
      <input
        type='text'
        value={fullname}
        onChange={(e) => {
          setFullname(e.target.value)
        }}
        name='fullname'
        className='bg-white rounded-lg mt-2 py-2 px-4 focus:outline-none '
      />


      <label
        className='mt-4'
      >
        {t('contact-form-email')}<span className='text-red'>*</span>
      </label>
      <input
        type='email'
        name='email'
        value={email}
        onChange={(e) => {
          setEmail(e.target.value)
        }}
        className='bg-white rounded-lg mt-2 py-2 px-4 focus:outline-none '
      />


      <label
        className='mt-4'
      >
        {t('contact-form-subject')}<span className='text-red'>*</span>
      </label>
      <input
        type='text'
        name='subject'
        value={subject}
        onChange={(e) => {
          setSubject(e.target.value)
        }}
        className='bg-white rounded-lg mt-2 py-2 px-4 focus:outline-none '
      />

      <label
        className='mt-4'
      >
        {t('contact-form-message')}<span className='text-red'>*</span>
      </label>
      <textarea
        name='message'
        value={message}
        onChange={(e) => {
          setMessage(e.target.value)
        }}
        className='bg-white rounded-lg mt-2 py-2 px-4 focus:outline-none '
      ></textarea>

      <ButtonPink
        as={'button'}
        buttonProps={{
          type: 'submit',
        }}
        type='submit'
        className='mt-8 min-w-min w-min'
      >
        {t('contact-form-submit')}
      </ButtonPink>
      {
        showSuccessMessage &&
        <p className='mt-6 font-bold bg-green p-3 text-white rounded-lg'>
          {t('contact-form-success')}
        </p>
      }
      {
        showFailureMessage &&
        <p className='mt-6 font-bold bg-red p-3 text-white rounded-lg'>
          {t('contact-form-error')}
        </p>
      }
    </form>
  )
}
