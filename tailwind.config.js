module.exports = {
  purge: [
    './src/**/*.{js,jsx,ts,tsx}',
    './public/index.html',
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      display: ['"Amatic SC"',],
      base: ['"Open Sans"',],
      sans: ['"Open Sans"',],
    },
    extend: {
      colors: {
        light: '#f5f5f5',
        dark: '#212121',
        white: {
          lightest: '#ffffff',
          DEFAULT: '#e2e2ca',
        },
        black: '#000000',
        yellow: '#fecb02',
        orange: '#f57d31',
        green: {
          lighter: '#afc096',
          light: '#2f766a',
          DEFAULT: '#196560',
        },
        blue: {
          light: '#b4c8c6',
          DEFAULT: '#0d3e5e',
        },
        red: '#e0333f',
        pink: {
          light: '#e4d4dc',
          DEFAULT: '#e0c5d0',
        },
        gray: {
          DEFAULT: '#a09ba1',
        },
        brown: {
          light: '#e0c991',
          DEFAULT: '#c0ac98',
        },
      },
      aspectRatio: {
        '9/16': '9 / 16',
        '3/4': '3 / 4',
      },
    },
  },
  variants: {
    extend: {
      'zIndex': ['hover',],
      'borderRadius': ['hover',],
    },
  },
  plugins: [],
}
