import sendgrid from '@sendgrid/mail'


sendgrid.setApiKey(process.env.SENDGRID_API_KEY)


async function sendEmail(req, res) {
  try {
    console.log('REQ.BODY', req.body)
    await sendgrid.send({
      to: 'gabisimon+francaceramicas@hotmail.com',
      from: 'no-reply@francaceramicas.com',
      subject: `${req.body.subject}`,
      html: `<p>${req.body.message}</p><br/><br/><p>${req.body.fullname} <${req.body.email}></p>`,
    })

  } catch (error) {
    return res.status(error.statusCode || 500).json({ error: error.message })

  }

  return res.status(200).json({ error: '' })
}

export default sendEmail
